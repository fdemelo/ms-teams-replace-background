import numpy as np
import tensorflow as tf
v_tf = tf.__version__.split('.')[0]
if v_tf == '2':
    import tensorflow.compat.v1 as tf
    tf.disable_v2_behavior()
import imutils
import os
import cv2
from lib.helper import FPS2, WebcamVideoStream
from skimage import measure
from random import randint
import cv2
import time
import pyfakewebcam
import numpy as np
import argparse

img_num=0
mike_flag=True

parser = argparse.ArgumentParser()
parser.add_argument('--width', type=int, default=640, help='The width of you webcam footage.')
parser.add_argument('--height', type=int, default=480, help='The height of you webcam footage.')
parser.add_argument('--image', type=str, default='office.jpeg', help='The path to background image.')
parser.add_argument('--input-stream', type=int, default=0, help='The number of the video device, where webcam is recorded. E.g. if /dev/video0 is input stream, put 0 as a parameter.')
parser.add_argument('--output-stream', type=int, default='2', help='The number of the video device, where the modified video is streamed. E.g. if /dev/video2 is output stream, put 2 as a parameter.')
opt = parser.parse_args()

IMG_W = opt.width
IMG_H = opt.height
background_path = opt.image
segmented_cam = pyfakewebcam.FakeWebcam("/dev/video" + str(opt.output_stream), IMG_W, IMG_H)

def load_model():
    print('Loading model...')
    detection_graph = tf.Graph()
    with detection_graph.as_default():
        seg_graph_def = tf.GraphDef()
        with tf.gfile.GFile('models/deeplabv3_mnv2_pascal_train_aug/frozen_inference_graph.pb', 'rb') as fid:
            serialized_graph = fid.read()
            seg_graph_def.ParseFromString(serialized_graph)
            tf.import_graph_def(seg_graph_def, name='')
    return detection_graph


def segmentation(detection_graph):

    vs = WebcamVideoStream(int(opt.input_stream), IMG_W, IMG_H).start()

    resize_ratio = 1.0 * 513 / max(vs.real_width, vs.real_height)
    target_size = (int(resize_ratio * vs.real_width),
                   int(resize_ratio * vs.real_height))
    config = tf.ConfigProto(allow_soft_placement=True)
    config.gpu_options.allow_growth = True
    resized_background_image = cv2.resize(cv2.imread(background_path), target_size)
    print("Starting...")
    
    with detection_graph.as_default():
        with tf.Session(graph=detection_graph) as sess:
            while vs.isActive():
                image = cv2.resize(vs.read(), target_size)
                batch_seg_map = sess.run('SemanticPredictions:0',
                                         feed_dict={'ImageTensor:0': [cv2.cvtColor(image, cv2.COLOR_BGR2RGB)]})
                seg_map = batch_seg_map[0]
                seg_map[seg_map != 15] = 0

                bg_copy = resized_background_image.copy()

                mask = (seg_map == 15)
                bg_copy[mask] = image[mask]

                seg_image = np.stack(
                    (seg_map, seg_map, seg_map), axis=-1).astype(np.uint8)
                gray = cv2.cvtColor(seg_image, cv2.COLOR_BGR2GRAY)

                thresh = cv2.threshold(gray, 10, 255, cv2.THRESH_BINARY)[1]
                major = cv2.__version__.split('.')[0]
                
                ir = cv2.resize(bg_copy, (vs.real_width, vs.real_height))
                segmented_cam.schedule_frame(ir[:,:,::-1])
    fps.stop()
    vs.stop()
    cv2.destroyAllWindows()


if __name__ == '__main__':
    graph = load_model()
    segmentation(graph)
