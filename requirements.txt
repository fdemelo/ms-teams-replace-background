scikit_image==0.17.2
pyfakewebcam==0.1.0
opencv_python==4.2.0.32
imutils==0.5.3
tensorflow_gpu==1.15.3
numpy==1.18.3
skimage==0.0
tensorflow==2.4.0
